import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeLobbyComponent } from './welcome-lobby.component';


const routes: Routes = [
  {
    path:'',
    component:WelcomeLobbyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeLobbyRoutingModule { }
