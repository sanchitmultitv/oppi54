import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
declare var $: any;
import * as _ from 'underscore';
import * as Clappr from 'clappr';
@Component({
  selector: 'app-auditorium-one',
  templateUrl: './auditorium-one.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss'],
  animations: [fadeAnimation],

})
export class AuditoriumOneComponent implements OnInit, OnDestroy {
  videoEnd = false;
  // videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  interval;
  like = false;
  player:any
  actives: any = [];
  newWidth;
  datas:any;
  newHeight;
  vidURL:any
  // stream: any;
  //  videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session1.smil/playlist.m3u8';
  videoPlayer:any
  qaList: any;
  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.audiActive();
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
  //  this.playVideo();

    // console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.chatService.getconnect('toujeo-141');
    this.chatService.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'groupchat') {
        this.chatGroup();
      }

    }));
  }
  
  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      // console.log(res, 'resssss');
      // this.actives = res.result;
      this.actives = res.result;
      this.videoPlayer = res.result;
      this.vidURL = this.actives[0].stream
      console.log(this.vidURL);
      this.playVideo()
      // console.log(this.videoPlayer+ "YO YO HONEY SINGH")
      // console.log(this.actives[0]);
    })
  }
  playVideo() {
    
    console.log(this.vidURL);
    var playerElement = document.getElementById("player_sec");
    this.player = new Clappr.Player({
      parentId: 'player_sec',
      source: this.vidURL,
      // source: "http://d3tn0h9cityxqm.cloudfront.net/streamline/chunks/701_5ef2e2fc9223a/701_5ef2e2fc9223a_master.m3u8",
      poster: 'assets/sat.jpg',
      height: '56.8%',
      maxBufferLength: 30,
      width: '51.4%',
      autoPlay: true,
      loop: true,
      hideMediaControl: false,
      hideVolumeBar: false,
      hideSeekBar: false,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '0'});
    // if (window.innerWidth <= 572) {
    //   this.player.play();
    //   this.player.resize({ width: '100%', height: window.innerWidth / 2 + 30 });
    // } else {
    //   this.player.play();
    //   var aspectRatio = 9 / 16;
    //   // tslint:disable-next-line: prefer-const
    //   this.newWidth = document.getElementById('player-wrapper').parentElement
    //     .offsetWidth;
    //   this.newHeight = 2 * Math.round((this.newWidth * aspectRatio) / 2);
    //   if (this.newWidth < 1000) {
    //   this.player.resize({ width: this.newWidth, height: this.newHeight });
    //   $('.container')
    //     .find('.player-poster')
    //     .css('background-size', 'contain');
    // }
    // }
  }
  videoEnded() {
    this.videoEnd = true;
  }
  getHeartbeat(){
    // alert("ss")
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '141');
    formData.append('audi', '1');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioHoot() {
    let playaudio: any = document.getElementById('myAudioHoot');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData();
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'oppinew';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-141');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }

  closePopup() {
    $('.groupchatOne').modal('hide');
  }
  likeopen(){
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  postMessage(value) {

    let data = JSON.parse(localStorage.getItem('virtual'));
    this.datas = data.id;
    this.datas = data.name;

    const formData = new FormData();
    formData.append('room_name', this.roomName);
    formData.append('user_name', data.name);
    formData.append('email', data.email);
    formData.append('chat_data', value);
    formData.append('is_approved', '1');
    formData.append('event_id', '140');
    formData.append('created', '2020-12-10 18:18:18');

    this._fd.postGroup(formData).subscribe(res => {
      console.log('res', res);
      let arr = {
        'user_id': data.id,
        'user_name': data.name,
        'room_name':this.roomName,
        'email':data.email,
        'created': '2020-12-10 18:18:18',
        'chat_data':value,
        'is_approved': '1'
      };
      this.messageList.push(arr);
      // this.messageList = res.result;
    });
    this.textMessage.reset();
    

  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}

