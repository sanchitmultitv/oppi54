import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumOneComponent } from './auditorium-one.component';


const routes: Routes = [  {path:'', component:AuditoriumOneComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumOneRoutingModule { }
