import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditoriumFourRoutingModule } from './auditorium-four-routing.module';
import { AuditoriumfourComponent } from './auditoriumfour.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgStreamingModule } from 'videogular2/compiled/streaming';


@NgModule({
  declarations: [AuditoriumfourComponent],
  imports: [
    CommonModule,
    AuditoriumFourRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule
  ]
})
export class AuditoriumFourModule { }
