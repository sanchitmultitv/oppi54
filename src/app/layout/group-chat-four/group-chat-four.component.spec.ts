import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupChatFourComponent } from './group-chat-four.component';

describe('GroupChatFourComponent', () => {
  let component: GroupChatFourComponent;
  let fixture: ComponentFixture<GroupChatFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupChatFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupChatFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
